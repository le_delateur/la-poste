package poste;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
public class CourrielTest {
	ArrayList<String> truc=new ArrayList<>();
	
	Courriel mail1=new Courriel("thomas@gratou.LOL","PTDR","Salut c'est manalux vous aimez le chocolat?",truc);
	
	
	@Test
	public void mailValideTest() {
		assertTrue(mail1.mailValide());
	}

	
	@Test
	public void mailnonValideTestAro() {
		mail1.set_adr("grougroyu.lol");
		assertFalse(mail1.mailValide());
		
	}
	@Test
	public void mailnonValideTestDot() {
		mail1.set_adr("groug@royu");
		assertFalse(mail1.mailValide());
	}
	
	@Test
	public void mailnonValideTestAL() {
		mail1.set_adr("/grouyu@qzdqz.lol");
		assertFalse(mail1.mailValide());
	}
	
	
	
	
	
	@Test
	public void titreValideTest() {
		assertTrue(mail1.titreValide());
	}
	
	@Test
	public void titreValideTestVide() {
		mail1.set_titre("");
		assertFalse(mail1.titreValide());
	}
	@Test
	public void titreValideTestNull() {
		Courriel mail2=new Courriel();
		assertFalse(mail2.titreValide());
	}
	
	
	@Test
	public void messageCont() {
		truc.add("oui ");
		assertTrue(mail1.messageCont());
	}
	@Test
	public void messageContJP() {
		truc.add("oui");
		mail1.set_message("zqdzdqz zqdq zd q");
		assertFalse(mail1.messageCont());
		
	}
	
	

	
	@ParameterizedTest
	@ValueSource(strings= {"ouibonjour@djhjj.lol","lalaop@sefsef.xd"})
	public void adressTestTrue(String o) {
		Courriel mailn= new Courriel(o);
		assertTrue(mailn.mailValide());
	}
	@ParameterizedTest
	@ValueSource(strings= {"qzkdqzdqzd.lel","qzdjIuFd@qdzd","qzdzdqzd"})
	public void adressTestFalse(String o) {
		Courriel mailn= new Courriel(o);
		assertFalse(mailn.mailValide());
	}
	
	
	@ParameterizedTest(name="{index} => adr={0},raison={1},correct={2}")
	@MethodSource("addrProvider")
	void adressTestM(String adr,String r,boolean b) {
		Courriel mailnew=new Courriel(adr);
		if (b){
			assertTrue(mailnew.mailValide());
			
		}
		else {
			assertFalse(mailnew.mailValide(), r);
			
		}
	}
	
	private static Stream<Arguments> addrProvider(){
		return Stream.of(
				Arguments.of("ouibonjour@djdj.lld","c'est bon",true),
				Arguments.of("lalaippo@zdz.xd","c'est bon",true),
				Arguments.of("qsqs@zdz","manque le point",false),
				Arguments.of("qsqs.zdqzd","manque le @",false),
				Arguments.of("dqzd","manque le @ et le .",false));
	}
	
	
	
	
	
	
	
	
}