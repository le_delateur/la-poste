
package poste;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class Colistest{
	Colis col1=new Colis("origine1","destination1","codepostal1",1,1f,Recommandation.un,"ddd",1);
	Colis col2=new Colis("origine1","destination1","codepostal1",10,0.126f,Recommandation.deux,"ddd",10);
	Colis col3=new Colis("origine1","destination1","codepostal1",10,5,Recommandation.zero,"ddd",10);
	@Test
	public void  tarifremb(){
		assertEquals(0.1f,col1.tarifRemboursement());
		assertEquals(5f,col2.tarifRemboursement());
		assertEquals(0f,col3.tarifRemboursement());
	}

	@Test
	public void tarifAff() {
		assertEquals(5.5,col1.tarifAffranchissement());
		assertEquals(6.5,col2.tarifAffranchissement());
	}
}