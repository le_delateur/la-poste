package poste;

import java.util.ArrayList;

public class Courriel {
	private String adrElec;
	private String titre;
	private String message;
	private ArrayList<String> pieceJoint;
	
	public Courriel(String adrElec,String titre,String message,ArrayList<String> pieceJoint) {
		this.adrElec=adrElec;
		this.titre=titre;
		this.message=message;
		this.pieceJoint=pieceJoint;
		
	}
	
	
	public Courriel(String adrElec) {
		this.adrElec=adrElec;
	}
	public Courriel() {
		this.pieceJoint=new ArrayList<String>();
	}
	
	
	public void set_adr(String adr) {
		adrElec=adr;
		return;				
	}
	public void set_titre(String tit) {
		titre=tit;
		return;
	}
	public void set_message(String mes) {
		message=mes;
		return;
	}
	public void set_pieceJoint(ArrayList<String> piece) {
		pieceJoint=piece;
		return;
	}
	
	
	
	
	
	public boolean envoyer() {
		 return this.titreValide()	&& this.mailValide() && this.messageCont();
		
	}
	
	public boolean titreValide() {
		return !(titre == null || titre=="") ;
	}
	
	public boolean mailValide() {
		return adrElec.matches("(\\p{Alnum}*)\\@(\\p{Alnum}*)\\.(\\p{Alnum}*)");
	}
	
	public boolean messageCont(){
		return (message.matches("(.*)\\b(Pj|joint|jointe)\\b(.*)") && !pieceJoint.isEmpty());
			
	}
	
	
	
}